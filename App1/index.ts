import express from 'express';

// define the server port
const PORT: number = 6060;

// create the server
const app: express.Application = express();

// configure server
app.use(express.json());
app.use(express.urlencoded());

// functions
let sumFunction = (number1: any, number2: any) => {
    return (number1 + number2);
}

let substractFunction = (number1: any, number2: any) => {
    return (number1 - number2);
}

let multiplyFunction = (number1: any, number2: any) => {
    return (number1 * number2);
}

let divideFunction = (number1: any, number2: any) => {
    return (number1 / number2);
}

// define routes
app.get('/', (request: express.Request, response: express.Response) => {
    response.send('Hello!');
});

app.post('/sum', (request: express.Request, response: express.Response) => {
    let res = sumFunction(+request.body.number1, +request.body.number2);
    response.send(`Result: ${request.body.number1} + ${request.body.number2} = ${res}`);
});

app.post('/substract', (request: express.Request, response: express.Response) => {
    let res = substractFunction(+request.body.number1, +request.body.number2);
    response.send(`Result: ${request.body.number1} - ${request.body.number2} = ${res}`);
});

app.post('/multiply', (request: express.Request, response: express.Response) => {
    let res = multiplyFunction(+request.body.number1, +request.body.number2);
    response.send(`Result: ${request.body.number1} * ${request.body.number2} = ${res}`);
});

app.post('/divide', (request: express.Request, response: express.Response) => {
    let res = divideFunction(+request.body.number1, +request.body.number2);
    response.send(`Result: ${request.body.number1} / ${request.body.number2} = ${res}`);
});

// start the server on the specified port
app.listen(PORT, () => {
    console.log(`Server started at http://localhost:${PORT}!`);
});
